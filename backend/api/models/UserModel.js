"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userSchema = new Schema({
  first_name: {
    type: String
  },
  last_name: {
    type: String
  },
  phone: {
    type: Number,
    default: ""
  },
  wallet: {
    type: String,
    default: ""
  },
  country:{
    type: String,
    default: ""
  },
  state:{
    type: String,
    default: ""
  },
  city: {
    type: String,
    default: ""
  },
  street: {
    type: String,
    default: ""
  },
  email: {
    type: String
  },
  image: {
    type: String,
    data: Buffer,
    default: ""
  },
  password: {
    type: String
  },
  email_verified: {
    type: Boolean,
    default: false
  },
  verification_link: {
    type: String,
    default: ""
  },
  reset_password_token: {
    type: String,
    default: ""
  },
  reset_password_expires: {
    type: Date,
    default: ""
  },
  status: {
  type: String,
  default: "1"
},
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }

});


var cardSchema=new Schema({
   user_id: {
    type: String
  },
  card_name: {
    type: String
  },
  card_no: {
    type: String
  },
  exp_month: {
    type: Number,
    default: ""
  },
  exp_year: {
    type: String,
    default: ""
  },
  cvv:{
    type: String,
    default: ""
  }

});



module.exports = mongoose.model("user", userSchema);
module.exports = mongoose.model("card", cardSchema);
