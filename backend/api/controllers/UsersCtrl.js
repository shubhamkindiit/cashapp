"use strict";

var mongoose = require("mongoose"),
	User = mongoose.model("user"),
	Card = mongoose.model("card"),
	path = require("path"),
	fs = require("fs"),
  multer = require("multer");

var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
var jwt = require("jsonwebtoken");
var config = require("../../config/default.json");
var statuscode = require("../../config/statuscodes.json");
var crypto = require("crypto");
var bcrypt = require("bcryptjs");
var jwtToken = "Cashapptoken";
let csc = require('country-state-city').default;
var errors= ['',null,undefined,'null','undefined',0];
var stripe = require('stripe')('sk_test_51IxqwqDRqWq4J1reAqrZYK2M2G4vCkCas8usgd6YEVf4BCDXdkkoHZl4UPhqmAFrpDlB5FLMMDNS34Azzk59HBcW00g1GeZFx6');
var transporter = nodemailer.createTransport(
	smtpTransport({
		service: "gmail",
		auth: {
			user: "cashifyapp233@gmail.com",
			pass: "Indiit@123"
		}
	})
);


/************UPLOADING IMAGE************ */


var storage = multer.diskStorage({
  destination: (req, file, cb) => {
      cb(null, "../images/");
  },
  filename: (req, file, cb) => {
      var fileName = file.originalname;
      cb(null, fileName.replace(/\s/g, ""));
  }
});
var upload = multer({ storage: storage }).single("image");

exports.uploadImage = function(req, res) {

  console.log('in backend:',req.body);


  upload(req, res, function(err) {
      if (err) {
          res.json({ error_code: statuscode.BAD_REQUEST, err_desc: err });
          return;
      }

      User.findOne({ _id: req.body.id, email_verified: true }, function(err, userdata) {
          if (userdata) {
            let base64Image = req.body.image;
              User.updateOne(
                  { _id: userdata._id },
                  {
                      $set: {
                          image: base64Image
                      }
                  },
                  { new: true },
                  function(err, updated) {
                      console.log("yes");
                      console.log(updated);
                      if(updated.nModified == 1){
                        User.findOne({ _id: userdata._id , email_verified: true }, function(err, userdat) {
                          if(userdat){
                            res.json({ status: statuscode.SUCCESS, msg: "Image updated Successfully!", data: userdat });
                          }
                        });
                      }
                  }
              );
          } else {
              res.json({ status: statuscode.UNAUTHORIZED, msg: "Sorry! No user exists!", data: "" });
          }
      });
  });
};


/************DELETING IMAGE************ */


exports.deleteImage = function(req, res) {
  upload(req, res, function(err) {
    if (err) {
        res.json({ error_code: statuscode.BAD_REQUEST, err_desc: err });
        return;
    }

    User.findOne({ _id: req.body.id, email_verified: true }, function(err, userdata) {
        if (userdata) {
          // let base64Image = 'data:image/jpeg;base64,' + req.body.image;
            User.updateOne(
                { _id: userdata._id },
                {
                    $set: {
                        image: req.body.image
                    }
                },
                { new: true },
                function(err, updated) {
                    console.log("yes");
                    console.log(updated);
                    if(updated.nModified == 1){
                      User.findOne({ _id: userdata._id , email_verified: true }, function(err, userdat) {
                        if(userdat){
                          res.json({ status: statuscode.SUCCESS, msg: "Image deleted Successfully!", data: userdat });
                        }
                      });
                    }
                }
            );
        } else {
            res.json({ status: statuscode.UNAUTHORIZED, msg: "Sorry! No user exists!", data: "" });
        }
    });
});
};


/************LOGIN************ */

exports.login_user = async ({ body }, res) => {
	try {
		User.findOne({ email: body.email, email_verified: true }, function(err, userexist) {
			if (userexist) {
				const passwordmatch = bcrypt.compareSync(body.password, userexist.password);
				console.log(passwordmatch);
				if (passwordmatch) {
					res.json({ status: statuscode.SUCCESS, msg: "Login Successfully!", data: userexist });
				} else {
					res.json({ status: statuscode.UNAUTHORIZED, msg: "Incorrect Password" });
				}
			} else {
				res.json({ status: statuscode.UNAUTHORIZED, msg: "Sorry! No user exist with this email!" });
			}
		});
	} catch (e) {
		res.json({ status: statuscode.SERVER_ERROR, msg: "Server Error!" });
	}
};

/*************SIGNUP************/

exports.add_user = async ({ body }, res) => {
	console.log(body);
	User.findOne({ email: body.email }, function(usererr, userexist) {


		if (userdata) {
    console.log('work start');
    console.log(userexist);
		var data = JSON.stringify(userexist);
		console.log(JSON.parse(data));
		var userdata = JSON.parse(data);
    console.log('work start2');
			if (userdata.email_verified == false) {
				console.log("entered update mode");
				var salt = bcrypt.genSaltSync(10);
				var hash = bcrypt.hashSync(body.password, salt);

				User.updateOne(
					{ _id: userdata._id },
					{
						$set: {
							first_name: body.firstName,
							last_name: body.lastName,
							email: body.email,
							password: hash,
							email_verified: false
						}
					},
					{ new: true },
					function(err, updated) {
						console.log("yes");
						console.log(updated);
					}
				);
				res.json({
					error: null,
					status: statuscode.SUCCESS,
					data: userdata,
					msg: "We have sent a link to your Email, Please verify First!"
				});
			} else {
				res.json({
					statuscode: statuscode.BAD_REQUEST,
					msg: "User with this email already exist! Try different Email!"
				});
			}
		} else {
			var salt = bcrypt.genSaltSync(10);
			var hash = bcrypt.hashSync(body.password, salt);

			var new_user = new User({
				first_name: body.firstName,
				last_name: body.lastName,
				email: body.email,
				password: hash
			});

			new_user.save(function(err, user) {
				if (user == null) {
					res.json({
						error: err,
						status: statuscode.SUCCESS,
						data: null
					});
				} else {
					console.log("sending mail");
					sendVerificationEmail(body, user);

					res.json({
						error: null,
						status: statuscode.SUCCESS,
						data: user,
						msg: "We have sent a link to your Email, Please verify First!"
					});
				}
			});
		}
	});
};

/************SEND VERIFICATION EMAIL************ */

function sendVerificationEmail(body, user) {
	console.log(body.email);
	console.log("enter sendemail");
	const credentials = {
		email: body.email,
		time: new Date().getTime()
	};
	console.log(credentials);
	const token = jwt.sign(credentials, jwtToken, { algorithm: "HS256" });
	console.log(token);
	User.updateOne(
		{ _id: user._id },
		{
			$set: {
				verification_link: token
			}
		},
		{ new: true },
		function(err, updated) {
			console.log(updated);
		}
	);

	const mailOptions = {
		from: '"CashApp 🖐" <cashifyapp233@gmail.com>',
		to: body.email,
		subject: "Verification Email ✔",
		text: "CashApp ",
		html: `
          <br>
          <p>Hey <br>Click the big blue button below to verify your email address.</p>

          <table border="0" cellspacing="0" cellpadding="0">
              <tr>
                  <td align="center" style="border-radius: 3px;" bgcolor="#0000FF"><a href="${config.baseUrl}mailverification/${token}" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 24px; border-radius: 2px; border: 1px solid #FFA73B; display: inline-block;">Verify email</a></td>
          </tr>
          </table>
          <br>
          <br>
          <br>
          Email sent by CashApp.<br>
          CashApp. <br>
          All rights reserved
          <br>
    `
	};
	transporter.sendMail(mailOptions, function(err, info) {
		if (err) console.log("Error detected " + err);
		else console.log(info);
	});
}

/******SHOW TEMPLATE IF EMAIL IS VERIFIED OR NOT****** */

exports.mailverification = async (req, res) => {
	try {
		const verificationlink = req.params.verificationLink;
		let user = await User.findOne({ verification_link: verificationlink });

		console.log(user.email_verified);
		if (user.email_verified == false) {
			User.updateOne(
				{ _id: user._id },
				{
					$set: {
						email_verified: true
					}
				},
				{ new: true },
				function(err, updated) {
					console.log(updated);
					res.write("<b>Your Email has been Verified !!</b>");
				}
			);
		} else {
			User.updateOne(
				{ _id: user._id },
				{
					$set: {
						email_verified: true
					}
				},
				{ new: true },
				function(err, updated) {
					console.log(updated);
					res.write("<b>Your Email has already been Verified !!</b>");
				}
			);
		}
	} catch (e) {
		res.write("<b>Something Went Wrong!</b>");
	}
};

/************GET BY USERID************ */

exports.getByUserId = async ({ body }, res) => {
	try {
		User.findOne({ _id: body.user_id }, function(err, userdata) {
			if (userdata) {
				res.json({ status: statuscode.SUCCESS, msg: "Fetched Successfully!", data: userdata });
			} else {
				res.json({ staus: statuscode.UNAUTHORIZED, msg: "Sorry! No user exist!", data: "" });
			}
		});
	} catch (e) {
		res.json({ status: statuscode.SERVER_ERROR, msg: "Server Error!", data: ""  });
	}
};

/***************FORGOT PASSWORD*************** */

exports.forgotpassword = async ({ body }, res) => {
	try {
		let user = await User.findOne({ email: body.email });

		if (user) {
			if (user.email_verified == false) {
				res.json({
					status: statuscode.BAD_REQUEST,
					msg: "User not Registered with this email or the email has not been verified!"
				});
			} else {
				const credentials = {
					email: body.email,
					time: new Date().getTime()
				};

				const token = jwt.sign(credentials, jwtToken, { algorithm: "HS256" });
				let resettoken = crypto.randomBytes(32).toString("hex");
				console.log(body.email);
				User.updateOne(
					{ _id: user._id },
					{
						$set: {
							reset_password_token: resettoken,
							reset_password_expires: Date.now()
						}
					},
					{ new: true },
					function(err, updated) {
						console.log(updated);
					}
				);

				const mailOptions = {
					from: '"CashApp 🖐" <cashifyapp233@gmail.com>', // sender address
					to: body.email, // list of receivers
					subject: "Forgot password ☹", // Subject line
					text: "CashApp",
					html: `
          <br>
          <p>Hey ${user.first_name} <br>Click on the button below to reset your password.</p>

          <table border="0" cellspacing="0" cellpadding="0">
              <tr>
                  <td align="center" style="border-radius: 3px;" bgcolor="#0000FF"><a href="${config.baseUrl}reset/${resettoken}" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 24px; border-radius: 2px; border: 1px solid #FFA73B; display: inline-block;">Reset Password</a></td>
          </tr>
          </table>
          <br>
          <br>
          <br>
          Email sent by CashAPP.<br>
          All rights reserved
          <br>
          `
				};

				transporter.sendMail(mailOptions, function(err, info) {
					if (err) console.log(err);
					else res.json({ status: statuscode.SUCCESS, msg: "Email has been sent, please check your email!" });
				});
			}
		} else {
			res.json({ status: statuscode.BAD_REQUEST, msg: "User not Registered with this Email!" });
		}
	} catch (e) {
		res.json({ status: statuscode.SERVER_ERROR, msg: "Something went wrong!" });
	}
};

/******************RESET PASSWORD TEMPLATE********* */

exports.reset = function(req, res) {
	console.log(req.params.token);

	User.findOne({ reset_password_token: req.params.token }, function(err, user) {
		console.log(user);
		if (!user) {
			console.log("IF");
			res.write("<b>Password reset token is invalid or has expired.<b>");
		} else {
			console.log("coming");
			fs.readFile("api/controllers/resetpassword.html", function(error, data) {
				console.log("is working");
				if (error) {
					console.log(error);
					res.writeHead(404);
					res.write("Contents you are looking for Not Found");
				} else {
					res.write(data);
				}
				res.end();
			});
		}
	});
};

/**********SET NEW PASSWORD************ */

exports.resetpass = async (req, res) => {
	var salt = bcrypt.genSaltSync(10);
	var hash = bcrypt.hashSync(req.body.Password, salt);

	let user = await User.findOne({ reset_password_token: req.params.token });

	User.updateOne(
		{ reset_password_token: req.params.token },
		{
			$set: {
				reset_password_token: "",
				reset_password_expires: Date.now(),
				password: hash
			}
		},
		{ new: true },
		function(err, updated) {
			console.log(err);
			console.log(updated);

			const mailOptions = {
				from: '"CashApp 🖐" <demou0017@gmail.com>', // sender address
				to: user.email, // list of receivers
				subject: "Password Confirmation ✔", // Subject line
				text: "CashApp",
				html: `
          <br>
          <p>Hey ${user.first_name} <br>

          This is a confirmation that the password for your account ${user.email} has just been changed.</p>
          <br>
          <br>
          <br>
          Email sent by CashAPp.<br>
          CashApp <br>
          All rights reserved
          <br>
          `
			};

			transporter.sendMail(mailOptions, function(err, info) {
				if (err) console.log(err);
				else res.json({ status: statuscode.SUCCESS, msg: "Email has been sent, please check your email!" });
			});

			res.json({ status: statuscode.SUCCESS, msg: "Password has been changed Successfully!" });
		}
	);
};


/************GET COUNTRIES************ */
exports.sendmoneytowallet = async (req, res) => {
Card.findOne({ _id: req.body.cardid }, function(err, carddata) {
  if (carddata) {

		stripe.tokens.create(
		{
		card: {
		number: '4242424242424242',
		exp_month: 6,
		exp_year: 2022,
		cvc: '314',
		},

		},
		function(err, token) {
          if(errors.indexOf(token.id)==-1){
						stripe.customers.create(
						{
						'email' : 'ifootballmatch@gmail.com', // customer email id
						'source' : token.id, // stripe token generated by stripe.js
						'name' : 'harinder singh',
						'address' : {"city" :'khamano', "country" : "+1", "line1" : "khamano , rattom", "line2" : "khamano , ratton", "postal_code" :"12345", "state" : "fategarh sahib"}
						},
						function(err, customer) {
								if(errors.indexOf(customer)==-1){

								stripe.charges.create(
								{
								'currency' :'USD',
								'amount' :  Number(req.body.amount)*100,
								'description' : 'Match commison to admin',
								'customer' : customer.id,

								},	function(err, charge) {
					                  if(errors.indexOf(charge)==-1){


					                  }
						              });

									}
						});

          }

		});

}else
{
	res.json({ staus: statuscode.UNAUTHORIZED, msg: "Sorry! No user exist!", data: "" });
}


});




};
exports.getmycards = async (req, res) => {
	try {
 // var new_favourite = new Card({
 //      user_id : req.body.user_id,
 //      card_name : 'demo',
 //      card_no: '4111111111111111',
 //      exp_month:'12',
 //      exp_year:'2025',
 //      cvv:'121',
 //    });

 //    new_favourite.save(function(err,qry){

 //    });
		Card.find({ user_id: req.body.user_id }, function(err, userdata) {
			if (userdata) {
				res.json({ status: statuscode.SUCCESS, msg: "Fetched Successfully!", data: userdata });
			} else {
				res.json({ staus: statuscode.UNAUTHORIZED, msg: "Sorry! No user exist!", data: "" });
			}
		});
	} catch (e) {
		res.json({ status: statuscode.SERVER_ERROR, msg: "Server Error!" });
	}
};


exports.getCountries = async ({ body }, res) => {
	try {
    let countries = csc.getAllCountries();
    console.log(countries);
			if (countries) {
				res.json({ status: statuscode.SUCCESS, msg: "Fetched Successfully!", data: countries });
			} else {
				res.json({ staus: statuscode.UNAUTHORIZED, msg: "Sorry! Unable to fetch!", data: "" });
			}

	} catch (e) {
		res.json({ status: statuscode.SERVER_ERROR, msg: "Server error!" });
	}
};


/************GET STATES************ */

exports.getStates = async ({ body }, res) => {
  console.log(body);
	try {
    let states = csc.getStatesOfCountry(body.country);
    console.log(states);

			if (states) {
				res.json({ status: statuscode.SUCCESS, msg: "Fetched Successfully!", data: states });
			} else {
				res.json({ staus: statuscode.UNAUTHORIZED, msg: "Sorry! Unable to fetch!", data: "" });
			}

	} catch (e) {
		res.json({ status: statuscode.SERVER_ERROR, msg: "Server Error!" });
	}
};

/************GET CITIES************ */

exports.getCities = async ({ body }, res) => {
	try {

    let cities = csc.getCitiesOfState(body.country, body.state);
    console.log(cities);

			if (cities) {
				res.json({ status: statuscode.SUCCESS, msg: "Fetched Successfully!", data: cities });
			} else {
				res.json({ staus: statuscode.UNAUTHORIZED, msg: "Sorry! Unable to fetch!", data: "" });
			}

	} catch (e) {
		res.json({ status: statuscode.SERVER_ERROR, msg: "Server Error!" });
	}
};


/************CHANGE PASSWORD FROM PROFILE SETTINGS************ */

exports.changePassword = async ({ body }, res) => {
	try {
		User.findOne({ _id: body.user_id, email_verified: true }, function(err, userexist) {
			if (userexist) {
				const passwordmatch = bcrypt.compareSync(body.oldpassword, userexist.password);
				console.log(passwordmatch);
				if (passwordmatch) {
        console.log("entered update mode");
				var salt = bcrypt.genSaltSync(10);
				var hash = bcrypt.hashSync(body.password, salt);
        console.log(hash);
				User.updateOne(
					{ _id: body.user_id },
					{
						$set: {
							password: hash
						}
					},
					{ new: true },
					function(err, updated) {
						console.log("yes");
						console.log(updated);
					}
				);
				res.json({
					error: null,
					status: statuscode.SUCCESS,
					msg: "Password has been updated!"
				});
				} else {
					res.json({ status: statuscode.UNAUTHORIZED, msg: "Old Password does not match" });
				}
			} else {
				res.json({ status: statuscode.UNAUTHORIZED, msg: "Sorry! Data not found!" });
			}
		});
	} catch (e) {
		res.json({ status: statuscode.SERVER_ERROR, msg: "Server Error!" });
	}
};


/************GET USER PROFILE************ */

exports.updateProfile = async ({ body }, res) => {
	try {
		User.findOne({ _id: body.user_id }, function(err, userdata) {
			if (userdata) {
        console.log('updating');
        User.updateOne(
					{ _id: body.user_id },
					{
						$set: {
							first_name: body.firstName,
              last_name: body.lastName,
              phone: body.phone,
              country: body.country,
              state: body.state,
              city: body.city,
              street: body.street
						}
					},
					{ new: true },
					function(err, updated) {
						console.log("yes");
						console.log(updated);
					}
				);
				res.json({
					error: null,
					status: statuscode.SUCCESS,
					msg: "Profile has been updated!",
				});

			} else {
				res.json({ staus: statuscode.UNAUTHORIZED, msg: "Sorry! No data found!", data: "" });
			}
		});
	} catch (e) {
		res.json({ status: statuscode.SERVER_ERROR, msg: "Server Error!" });
	}
};
