"use strict";
module.exports = function(app) {
	var users = require("../controllers/UsersCtrl");

	app.route("/addUser").post(users.add_user);
	// app.route("/resendotp").post(users.resendotp);
	// app.route("/verifyotp").post(users.verifyotp);
	app.route("/mailverification/:verificationLink").get(users.mailverification);
	app.route("/forgotpassword").post(users.forgotpassword);
	app.route("/reset/:token").get(users.reset).post(users.resetpass);
	app.route("/login").post(users.login_user);
	app.route("/getByUserId").post(users.getByUserId);
  app.route("/getCountries").get(users.getCountries);
app.route("/getmycards").post(users.getmycards);
app.route("/sendmoneytowallet").post(users.sendmoneytowallet);
  app.route("/getStates").get(users.getStates);
  app.route("/getCities").post(users.getCities);
  app.route("/changePassword").post(users.changePassword);
  app.route("/updateProfile").post(users.updateProfile);
  app.route("/uploadImage").post(users.uploadImage);
  app.route("/deleteImage").post(users.deleteImage);

	// var category = require("../controllers/CategoriesCtrl");

	// app.route("/add_category").post(category.add_category);
	// app.route("/get_category").get(category.get_category);
	/***SUB CATEGORY*** */
	// app.route("/add_sub_category").post(category.add_sub_category);
	// app.route("/get_sub_category").post(category.get_sub_category);
	/***SAVE QA***/

	// app.route("/save_qa").post(category.save_qa);
	// app.route("/get_qa").post(category.get_qa);
};
