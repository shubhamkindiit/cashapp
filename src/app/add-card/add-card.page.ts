import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Stripe } from "@ionic-native/stripe/ngx";

@Component({
	selector: "app-add-card",
	templateUrl: "./add-card.page.html",
	styleUrls: [ "./add-card.page.scss" ]
})
export class AddCardPage implements OnInit {
	submitted = false;
	cardForm: FormGroup;
	constructor(private formBuilder: FormBuilder, private stripe: Stripe) {
		this.createCardForm();
	}

	ngOnInit() {}

	/*****GET AUTH CONTROLS FOR AUTH FORM****** */
	get form() {
		return this.cardForm.controls;
	}

	/******CREATE CARD FORM**** */
	createCardForm() {
		this.cardForm = this.formBuilder.group(
			{
				cardName: [ "", [ Validators.required ] ],
				number: [ "", [ Validators.required ] ],
				expMonth: [ "", [ Validators.required ] ],
				expYear: [ "", [ Validators.required ] ]
				// cvv: [ "", [ Validators.required ] ]
			},
			{}
		);
	}

	/***********RESET MY FORM******* */
	onReset() {
		this.submitted = false;
		this.cardForm.reset();
	}

	checkDigit(e) {
		var val = e.target.value;
		var v = val.replace(/\s+/g, "").replace(/[^0-9]/gi, "");
		var matches = v.match(/\d{4,16}/g);
		var match = (matches && matches[0]) || "";
		var parts = [];

		for (var i = 0, len = match.length; i < len; i += 4) {
			parts.push(match.substring(i, i + 4));
		}
		console.log(parts);

		if (parts.length) {
			return parts.join(" ");
		} else {
			return val;
		}
	}

	/******SAVE CARD DETAILS**** */
	validateCard: any = false;
	validateExpMonthYear: any = false;

	saveCard(value: any): void {
		console.log(this.cardForm);
		this.submitted = true;
		if (this.cardForm.invalid) {
			return;
		}

		this.stripe.setPublishableKey("pk_test_8ghnSkc1YDWnhEuVkhyhHNIc00qj96jauv");
		this.stripe.validateCardNumber(value.number).then((res) => {
			console.log(res);
		});
	}
}
