import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { AddCardPageRoutingModule } from "./add-card-routing.module";
import { AddCardPage } from "./add-card.page";
import { Stripe } from "@ionic-native/stripe/ngx";

@NgModule({
	imports: [ CommonModule, FormsModule, ReactiveFormsModule, IonicModule, AddCardPageRoutingModule ],
	declarations: [ AddCardPage ],
	providers: [ Stripe ]
})
export class AddCardPageModule {}
