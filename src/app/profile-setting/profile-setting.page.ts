import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { UserService } from '../services/user/user.service';
import { CameraPhoto, Plugins, CameraResultType, CameraSource, Capacitor, FilesystemDirectory } from '@capacitor/core';
import { ActionSheetController, Platform } from '@ionic/angular';

const { Camera, Filesystem, Storage} = Plugins;
@Component({
  selector: 'app-profile-setting',
  templateUrl: './profile-setting.page.html',
  styleUrls: ['./profile-setting.page.scss'],
})
export class ProfileSettingPage implements OnInit {

 /*********DECLARATION********** */
 profiletab: string = "Basic";

 imgURL;

 isSubmitprofile= false;
 isSubmitpass = false;

 userdata:any = "";


 profileForm: FormGroup;
 passwordForm: FormGroup;


 countries: any = '';
 countrySelected: any = 'IN';
 states: any = '';
 cities: any = '';
 selectedCountry: any = '';
 selectedState: any = '';

 base64img: any = '';

 @ViewChild('fileInput', {static: false}) fileInput: ElementRef;

 /*********DECLARATION********** */

  constructor(private fb:FormBuilder, private userService: UserService, private plt: Platform, private actionSheetController: ActionSheetController, private cdr: ChangeDetectorRef) {

    this.createPassForm();
    this.createProfileForm();
    this.getCountriesData();
  }

  ngOnInit() {
    this.userData();
  }



/************CREATE PROFILE AND PASS FORM************ */

createProfileForm(){
  this.profileForm = this.fb.group({
    firstName : ['', [Validators.required]],
    lastName : ['', [Validators.required]],
    phone: ['', [Validators.required]],
    country : ['',[Validators.required]],
    state : ['',[Validators.required]],
    city : ['',[Validators.required]],
    street : ['',[Validators.required]],
    // address : ['', [Validators.required]]
  },{});
}

createPassForm(){
  this.passwordForm = this.fb.group({
    oldpassword: ['', [Validators.required]],
    password: ['', [Validators.required, Validators.minLength(8) , Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,12}$')]],
    confirmPassword : ['',[Validators.required,this.equalto('password')]],
    // address : ['', [Validators.required]]
  },{});
}

/************CREATE PROFILE AND PASS FORM************ */

changeStatus(): void {
  setTimeout(() => {
    this.cdr.detectChanges();
  }, 1500);
}

/***********PASSWORD MATCHING********** */

equalto(fieldName): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
  const input = control.value;
  const isValid=control.root.value[fieldName]===input;
  if(!isValid){
  return { equalTo: {isValid} };
  }
  else
  {
  return null;
  }
  };
}
/***********PASSWORD MATCHING********** */




/************ FORM ON SUBMIT************ */

  profileEdit(value: any): void{
    console.log('this',value.country.cname, value.state.sname);
    console.log('profile edit form');
    this.isSubmitprofile = true;
    if(this.profileForm.invalid){
      return;
    }
    const data = {
      user_id: localStorage.getItem('id'),
      firstName: value.firstName,
      lastName:  value.lastName,
      phone: value.phone,
      country: value.country.cname,
      state: value.state.sname,
      city: value.city,
      street: value.street
    };

    this.userService.presentLoading();
    this.userService.postData(data,'updateProfile').subscribe((result) =>{
      this.isSubmitprofile = false;
      console.log(result);
      this.userService.stopLoading();

      if(result.status === 200){
        this.userService.presentToast(result.msg,'success');
      }
      else if(result.status === 400){
        this.userService.presentToast(result.msg,'danger');
      }
      else{
        this.userService.presentToast('Error while signing up! Please try later','danger');
      }
    },
    err => {
      this.isSubmitprofile = false;
      this.userService.stopLoading();
      this.userService.presentToast('Unable to send request, Please try again later','danger');
    }
    );


  }



  passwordEdit(value: any): void{
    console.log('password edit form');
    this.isSubmitpass = true;
    if(this.passwordForm.invalid){
      return;
    }
    const data = {
      oldpassword: value.oldpassword,
      password: value.password,
      user_id: localStorage.getItem('id')
    };

    this.userService.presentLoading();
    this.userService.postData(data,'changePassword').subscribe((result) =>{
      this.userService.stopLoading();
      console.log(result);
      if(result.status === 200){
        this.isSubmitpass = false;
        this.userService.presentToast(result.msg,'success');
        this.onReset();
      }
      else if(result.status === 401){
        this.isSubmitpass = false;
        this.userService.presentToast(result.msg,'danger');
        this.onReset();
      }
      else{
        this.isSubmitpass = false;
        this.userService.presentToast('Error found!','danger');
        this.onReset();
      }
    },
    err => {
      this.onReset();
      this.isSubmitpass = false;
      this.userService.stopLoading();
      this.userService.presentToast('Unable to send request, Please try again later','danger');
    }
    );


  }

  /***********RESET MY FORM******* */
  onReset() {
    this.isSubmitpass = false;
    this.passwordForm.reset();
  }


  /*****GET AUTH CONTROLS FOR REGISTER FORM****** */

  get passform() {
    return this.passwordForm.controls;
  }
  get proform() {
    return this.profileForm.controls;
  }

     /*****GET USER DATA****** */



  userData() {
    const data = {
      user_id: localStorage.getItem('id'),
    };
    this.userService.presentLoading();
    this.userService.postData(data,"getByUserId").subscribe((result) => {
      console.log(result);
      this.userService.stopLoading();
      if(result.status === 200 ){
        this.userService.presentToast(result.msg, 'success');
        this.userdata = result.data;
      }
      else if(result.status === 401 ){
        this.userService.presentToast(result.msg, 'danger');
        this.userdata = result.data;
      }
      else if(result.status === 500 ){
        this.userService.presentToast(result.msg, 'danger');
        this.userdata = result.data;
      }
      else{
        this.userService.presentToast(result.msg,'danger');
        this.userdata = result.data;
      }

    });
  }

   /*****GET COUNTRIES****** */

  getCountriesData(){
    this.userService.getCountries().subscribe((result) =>{
      console.log(result);
      if(result.status === 200 ){
        // this.userService.stopLoading();
        // this.userService.presentToast(result.msg, 'success');
        this.countries = result.data;
      }
      else if(result.status === 401 ){
        // this.userService.stopLoading();
        // this.userService.presentToast(result.msg, 'danger');
      }
      else if(result.status === 500 ){
        // this.userService.stopLoading();
        // this.userService.presentToast(result.msg, 'danger');
      }
      else{
        // this.userService.stopLoading();
        // this.userService.presentToast(result.msg,'danger');
      }
    })
  }

   /*****ON COUNTRY,STATE CHANGE****** */

    onCountryChange(){
      // console.log(this.profileForm.get('country').value.value);
      let data = {
        country: this.profileForm.get('country').value.value
      };
      this.getStatesData(data);

    }
    onStateChange(){

      let data = {
        country: this.profileForm.get('country').value.value,
        state: this.profileForm.get('state').value.value
      };
      console.log(data);
      this.getCitiesData(data);
    }
   /*****GET STATES****** */

  getStatesData(post){
    this.userService.presentLoading();
    this.userService.getStates(post).subscribe((result) =>{
      console.log(result);
      if(result.status === 200 ){
        this.userService.stopLoading();
        // this.userService.presentToast(result.msg, 'success');
        this.states = result.data;
      }
      else if(result.status === 401 ){
        this.userService.stopLoading();
        // this.userService.presentToast(result.msg, 'danger');
        // this.states = result.msg;
      }
      else if(result.status === 500 ){
        this.userService.stopLoading();
        // this.userService.presentToast(result.msg, 'danger');
        // this.countries = result.msg;
      }
      else{
        this.userService.stopLoading();
        // this.userService.presentToast(result.msg,'danger');
        // this.countries = result.msg;
      }
    })
  }


  /*****GET CITIES****** */

  getCitiesData(data){
    this.userService.presentLoading();
    this.userService.getCities(data).subscribe((result) =>{
      console.log(result);
      if(result.status === 200 ){
        this.userService.stopLoading();
        // this.userService.presentToast(result.msg, 'success');
        this.cities = result.data;
      }
      else if(result.status === 401 ){
        this.userService.stopLoading();
        // this.userService.presentToast(result.msg, 'danger');
        // this.states = result.msg;
      }
      else if(result.status === 500 ){
        this.userService.stopLoading();
        // this.userService.presentToast(result.msg, 'danger');
        // this.countries = result.msg;
      }
      else{
        this.userService.stopLoading();
        // this.userService.presentToast(result.msg,'danger');
        // this.countries = result.msg;
      }
    })
  }

/*****TAKE PICTURE****** */

// async takePhoto(){
//   const options = {
//     resultType: CameraResultType.Uri
//   };
//   const originalPhoto = await Camera.getPhoto(options);
//   const photoInTempStorage = await Filesystem.readFile({
//     path: originalPhoto.path
//   });

//   let date = new Date(),
//   time = date.getTime(),
//   fileName = time + '.jpeg';

//   await Filesystem.writeFile({
//     data: photoInTempStorage.data,
//     path: fileName,
//     directory: FilesystemDirectory.Data
//   })

//   const finalPhotoUri = await Filesystem.getUri({
//     directory: FilesystemDirectory.Data,
//     path: fileName
//   });

//   let photoPath = Capacitor.convertFileSrc(finalPhotoUri.uri);
//   console.log(photoPath);

// }


/*****BASE 64 TO BLOB****** */

b64toBlob(b64Data, contentType = '',sliceSize = 512){
  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for(let offset = 0; offset < byteCharacters.length; offset += sliceSize){
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);

    for(let i=0; i < slice.length; i++){
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, {type: contentType});
  return blob;
}


/*****ADD IMAGE****** */

async addImage(source: CameraSource){
  const image = await Camera.getPhoto({
    quality: 60,
    allowEditing: true,
    resultType: CameraResultType.Base64,
    source
  });
  // console.log(image);
  // const file = this.b64toBlob(image.base64String, `image/${image.format}`);
  // console.log(file);
  const reader = new FileReader();

  const id = localStorage.getItem('id');
  const formData = new FormData();

  formData.append("id",id);
  formData.append("image",'data:image/jpeg;base64,'+image.base64String);
  this.userService.presentLoading();
  this.userService.postData(formData, 'uploadImage').subscribe((result) =>{
    this.userService.stopLoading();

      if(result.status === 200){
        this.userService.presentToast(result.msg,'success');
        this.changeStatus();
        this.userdata = result.data;
        console.log('updated data:',result.data);
      }
      else if(result.status === 401){
        this.userService.presentToast(result.msg,'danger');
      }
      else{
        this.userService.presentToast('Error while signing up! Please try later','danger');
      }
  });

  reader.onprogress = (e) => {
    console.log(e);
  };

  reader.onload = (e) => {
    this.imgURL = reader.result;
  };

  // reader.readAsDataURL(file);
}


/***** EDIT  PROFILE ****** */

async editProfile(){
  console.log(this.plt.platforms);
  const buttons = [
    {
      text: 'Take Photo',
      icon: 'camera',
      handler: () =>{
        this.addImage(CameraSource.Camera);
      }
    },
    {
      text: 'Choose Photo From Library',
      icon: 'image',
      handler: () => {
        this.addImage(CameraSource.Photos);
      }
    },
    {
      text: 'Delete Photo',
      icon: 'trash',
      handler: () => {
        console.log('Delete image');
        const id = localStorage.getItem('id');
          const formData = new FormData();

          formData.append("id",id);
          formData.append("image",'');
        // console.log('Image empty', data);
        // this.userService.presentLoading();
        this.userService.postData(formData, 'deleteImage').subscribe((result) =>{
          // this.userService.stopLoading();
          if(result.status === 200){
            this.userService.presentToast(result.msg,'success');
            this.userdata = result.data;
            // setTimeout(() => {

            // }, 2000);
          }
          else if(result.status === 401){
            this.userService.presentToast(result.msg,'danger');
          }
          else{
            this.userService.presentToast('Error while signing up! Please try later','danger');
          }
        });
      }
    }
  ];

  // Only allow file selection inside a browser

  if(!this.plt.is('hybrid')) {
    buttons.push({
      text: 'Choose a File',
      icon: 'attach',
      handler: () =>{
        this.fileInput.nativeElement.click();
      }
    });
  }

  const actionSheet = await this.actionSheetController.create({
    header: 'Select Image Source',
    buttons
  });

  await actionSheet.present();

}

//convert to base64

filetob64(){
  const reader = new FileReader();
        reader.readAsDataURL(this.fileInput.nativeElement.files[0]);

        reader.onload = () => {
          this.base64img = reader.result;
          const id = localStorage.getItem('id');
          const formData = new FormData();
          formData.append("id",id);
          formData.append("image",this.base64img);
          this.userService.presentLoading();
    this.userService.postData(formData, 'uploadImage').subscribe((result) =>{
      this.userService.stopLoading();

        if(result.status === 200){
          this.userService.presentToast(result.msg,'success');
          this.changeStatus();
          this.userdata = result.data;
          console.log('updated data:',result.data);
        }
        else if(result.status === 401){
          this.userService.presentToast(result.msg,'danger');
        }
        else{
          this.userService.presentToast('Error while signing up! Please try later','danger');
        }
    });
        };
        reader.onerror = function(e){
          console.log(e);
        }
        console.log(this.base64img);


}

}
